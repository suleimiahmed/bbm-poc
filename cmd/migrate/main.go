package main

import (
	"os"
	"sort"

	"github.com/olekukonko/tablewriter"
	"gitlab.com/suleimiahmed/bbm-poc/app"
	"gitlab.com/suleimiahmed/bbm-poc/bbm"
	"gitlab.com/suleimiahmed/bbm-poc/cmd/migrate/migrations"
	"gitlab.com/suleimiahmed/bbm-poc/internal/config"
	"gitlab.com/suleimiahmed/bbm-poc/internal/datastore"
	"gitlab.com/suleimiahmed/bbm-poc/internal/logger"
)

func main() {

	// read env config
	cfg, err := config.LoadConfig(nil)
	if err != nil {
		panic(err)
	}
	// create a logger
	logger, err := logger.NewZapLogger()
	if err != nil {
		panic(err)
	}
	// create database client
	store, sqldb, err := datastore.NewDB(cfg, logger)
	if err != nil {
		logger.Panic(err.Error())
	}

	// register work with worker
	worker, err := app.RegisterWork(bbm.AllWork()...)

	if err != nil {
		panic(err)
	}
	mig := migrations.NewMigrator(store, sqldb, logger, worker, cfg.SyncBBM)
	_, err = mig.Up()
	if err != nil {
		logger.Panic(err.Error())
	}

	statuses, err := mig.Status()
	if err != nil {
		logger.Panic(err.Error())
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Migration", "Applied"})
	table.SetColWidth(80)

	// Display table rows sorted by migration ID
	var ids []string
	for id := range statuses {
		ids = append(ids, id)
	}
	sort.Strings(ids)

	for _, id := range ids {
		name := id
		if statuses[id].Unknown {
			name += " (unknown)"
		}

		if statuses[id].PostDeployment {
			name += " (post deployment)"
		}

		var appliedAt string
		if statuses[id].AppliedAt != nil {
			appliedAt = statuses[id].AppliedAt.String()
		}

		table.Append([]string{name, appliedAt})
	}

	table.Render()
}

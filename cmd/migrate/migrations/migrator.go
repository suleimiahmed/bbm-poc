package migrations

// mostly the exact same as https://gitlab.com/gitlab-org/container-registry/-/blob/master/registry/datastore/migrations/migrator.go?ref_type=heads with the exeption of `migrateWithBBMCheck`

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"go.uber.org/zap"

	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/suleimiahmed/bbm-poc/app"
	"gitlab.com/suleimiahmed/bbm-poc/internal/config"
	"gitlab.com/suleimiahmed/bbm-poc/internal/datastore"
)

const (
	migrationTableName = "schema_migrations"
	dialect            = "postgres"
)

func init() {
	migrate.SetTable(migrationTableName)
}

type Migrator struct {
	db     *sql.DB
	store  *datastore.PgDB
	logger *zap.Logger
	// BBMWorkFuncs injects the work function mapping
	BBMWorkFuncs *app.JobWorker
	BBMConfig    config.SyncBBM
	migrations   []*Migration

	skipPostDeployment bool
}

func NewMigrator(store *datastore.PgDB, db *sql.DB, logger *zap.Logger, jw *app.JobWorker, bbmCfg config.SyncBBM) *Migrator {
	m := &Migrator{
		db:           db,
		store:        store,
		logger:       logger,
		migrations:   allMigrations,
		BBMWorkFuncs: jw,
		BBMConfig:    bbmCfg,
	}

	return m
}

// MigratorOption enables the creation of functional options for the
// configuration of the migrator.
type MigratorOption func(m *Migrator)

// Source allows the migrator to use an alternative source of migrations, used
// for testing.
func Source(a []*Migration) func(m *Migrator) {
	return func(m *Migrator) {
		m.migrations = a
	}
}

// SkipPostDeployment configures the migration to not apply postdeployment migrations.
func SkipPostDeployment(m *Migrator) {
	m.skipPostDeployment = true
}

// Version returns the current applied migration version (if any).
func (m *Migrator) Version() (string, error) {
	records, err := migrate.GetMigrationRecords(m.db, dialect)
	if err != nil {
		return "", err
	}
	if len(records) == 0 {
		return "", nil
	}

	return records[len(records)-1].Id, nil
}

// LatestVersion identifies the version of the most recent migration in the repository (if any).
func (m *Migrator) LatestVersion() (string, error) {
	all, err := m.eligibleMigrations()
	if err != nil {
		return "", err
	}
	if len(all) == 0 {
		return "", nil
	}

	return all[len(all)-1].Id, nil
}

func (m *Migrator) migrate(direction migrate.MigrationDirection, limit int) (int, error) {
	src, err := m.eligibleMigrationSource()
	if err != nil {
		return 0, err
	}

	return migrate.ExecMax(m.db, dialect, src, direction, limit)
}

// Up applies all pending up migrations. Returns the number of applied migrations.
func (m *Migrator) Up() (int, error) {
	err := m.migrateWithBBMCheck()
	if err != nil {
		return 0, err
	}

	return len(m.migrations), nil
}

// InjectMigration hot loads extra migrations
func (m *Migrator) InjectMigration(extraMigrations []*Migration) {
	m.migrations = append(m.migrations, extraMigrations...)
}

// UpN applies up to n pending up migrations. All pending migrations will be applied if n is 0.  Returns the number of
// applied migrations.
func (m *Migrator) UpN(n int) (int, error) {
	return m.migrate(migrate.Up, n)
}

// UpNPlan plans up to n pending up migrations and returns the ordered list of migration IDs. All pending migrations
// will be planned if n is 0.
func (m *Migrator) UpNPlan(n int) ([]string, error) {
	return m.plan(migrate.Up, n)
}

// Down applies all pending down migrations.  Returns the number of applied migrations.
func (m *Migrator) Down() (int, error) {
	return m.migrate(migrate.Down, 0)
}

// DownN applies up to n pending down migrations. All migrations will be applied if n is 0.  Returns the number of
// applied migrations.
func (m *Migrator) DownN(n int) (int, error) {
	return m.migrate(migrate.Down, n)
}

// DownNPlan plans up to n pending down migrations and returns the ordered list of migration IDs. All pending migrations
// will be planned if n is 0.
func (m *Migrator) DownNPlan(n int) ([]string, error) {
	return m.plan(migrate.Down, n)
}

// migrationStatus represents the status of a migration. Unknown will be set to true if a migration was applied but is
// not known by the current build.
type migrationStatus struct {
	Unknown        bool
	PostDeployment bool
	AppliedAt      *time.Time
}

// Status returns the status of all migrations, indexed by migration ID.
func (m *Migrator) Status() (map[string]*migrationStatus, error) {
	applied, err := migrate.GetMigrationRecords(m.db, dialect)
	if err != nil {
		return nil, err
	}
	known, err := m.allMigrations()
	if err != nil {
		return nil, err
	}

	statuses := make(map[string]*migrationStatus, len(applied))
	for _, k := range known {
		statuses[k.Id] = &migrationStatus{}

		if mig := m.findMigrationByID(k.Id); mig != nil && mig.PostDeployment {
			statuses[k.Id].PostDeployment = true
		}
	}

	for _, m := range applied {
		if _, ok := statuses[m.Id]; !ok {
			statuses[m.Id] = &migrationStatus{Unknown: true}
		}

		statuses[m.Id].AppliedAt = &m.AppliedAt
	}

	return statuses, nil
}

// HasPending determines whether all known migrations are applied or not.
func (m *Migrator) HasPending() (bool, error) {
	records, err := migrate.GetMigrationRecords(m.db, dialect)
	if err != nil {
		return false, err
	}

	eligible, err := m.eligibleMigrations()
	if err != nil {
		return false, err
	}

	for _, k := range eligible {
		if !migrationApplied(records, k.Id) {
			return true, nil
		}
	}

	return false, nil
}

func (m *Migrator) plan(direction migrate.MigrationDirection, limit int) ([]string, error) {
	src, err := m.eligibleMigrationSource()
	if err != nil {
		return nil, err
	}

	planned, _, err := migrate.PlanMigration(m.db, dialect, src, direction, limit)
	if err != nil {
		return nil, err
	}

	result := make([]string, 0, len(planned))
	for _, m := range planned {
		result = append(result, m.Id)
	}

	return result, nil
}

func (m *Migrator) allMigrations() ([]*migrate.Migration, error) {
	return m.allMigrationSource().FindMigrations()
}

func (m *Migrator) allMigrationSource() *migrate.MemoryMigrationSource {
	src := &migrate.MemoryMigrationSource{}

	for _, migration := range m.migrations {
		src.Migrations = append(src.Migrations, migration.Migration)
	}

	return src
}

func (m *Migrator) eligibleMigrations() ([]*migrate.Migration, error) {
	src, err := m.eligibleMigrationSource()
	if err != nil {
		return nil, err
	}

	return src.FindMigrations()
}

func (m *Migrator) eligibleMigrationSource() (*migrate.MemoryMigrationSource, error) {
	src := &migrate.MemoryMigrationSource{}

	records, err := migrate.GetMigrationRecords(m.db, dialect)
	if err != nil {
		return src, err
	}

	for _, migration := range m.migrations {
		if m.skipPostDeployment && migration.PostDeployment &&
			// Do not skip already applied postdeployment migrations. The migration
			// library expects to see applied migrations when it plans a migration,
			// and we should ensure that down migrations affect all applied migrations.
			!migrationApplied(records, migration.Id) {
			continue
		}

		src.Migrations = append(src.Migrations, migration.Migration)
	}

	return src, nil
}

func migrationApplied(records []*migrate.MigrationRecord, id string) bool {
	for _, r := range records {
		if r.Id == id {
			return true
		}
	}

	return false
}

func (m *Migrator) findMigrationByID(id string) *Migration {
	for _, mig := range m.migrations {
		if mig.Id == id {
			return mig
		}
	}
	return nil
}

// migrateWithBBMCheck will run a database schema migration in steps until it reaches the latest version.
// if a database schema migration contains a bbm dependecy it will pause the database schema migration and attempt to run the bbm to completion first.
// upon completing the bbm it will proceed to the continue with the database schema migration and so on and so forth.
func (m *Migrator) migrateWithBBMCheck() error {
	src, err := m.eligibleMigrationSource()
	if err != nil {
		return err
	}

	// Get all already applied migrations
	migrationRecords, err := migrate.GetMigrationRecords(m.db, dialect)
	if err != nil {
		return err
	}
	// Convert applied migrations to map for easy lookup
	migrationRecordsMap := make(map[string]*migrate.MigrationRecord)
	for _, migrationRecord := range migrationRecords {
		migrationRecordsMap[migrationRecord.Id] = migrationRecord
	}

	// sort all the migrations in the src material by id
	sortedMigrations, err := src.FindMigrations()
	if err != nil {
		return err
	}

	// make a map for translating between pure migrations and our own version of migration
	localMigrationsMap := make(map[string]*Migration)
	for _, localMigrationRecord := range m.migrations {
		localMigrationsMap[localMigrationRecord.Id] = localMigrationRecord
	}

	for _, migration := range sortedMigrations {

		// if we've already run a migration skip it
		if _, ok := migrationRecordsMap[migration.Id]; ok {
			continue
		} else {
			// convert the migration to a local migration
			lmigration := localMigrationsMap[migration.Id]
			if len(lmigration.EnsureBBMBefore) > 0 {
				// Check if the required BBMs are completed
				for _, bbmName := range lmigration.EnsureBBMBefore {
					complete, err := app.IsBBMCompleteByName(context.Background(), m.store, bbmName)
					if err != nil {
						return err
					}
					if !complete {
						// BBM is not complete, start bbm
						err := m.findAndexecuteBBM(bbmName)
						if err != nil {
							return err
						}
					}
				}
			}

			// Migrate the current migration
			_, err := migrate.ExecVersion(m.db, dialect, src, migrate.Up, migration.VersionInt())
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// findAndexecuteBBM arrempts to acquire the BBM lock and run all BBM jobs to completion in one shot (without releasing the lock), one job at a time.
func (m *Migrator) findAndexecuteBBM(name string) error {

	// try to get the lock
	for {
		m.logger.Sugar().Infof("Searching for BBM: %s", name)

		if acquireLock(m.store, m.BBMConfig.BBMLockAcquistionInterval, app.LockID, m.BBMConfig.BBMMaxLockAcquistionAttempts) {
			defer app.Unlock(context.Background(), m.store, app.LockID)
			break
		} else {
			return fmt.Errorf("failed to acquire lock for BBM after %d attempts", m.BBMConfig.BBMMaxLockAcquistionAttempts)
		}
	}

	// once a lock is obtained, start finding and executing job batches until there are no more jobs to run for the BBM.
	for {
		job, err := app.FindExecutableJobForBBM(m.store, m.logger, name, m.BBMConfig.BBMMaxJobAttempts)
		if err != nil {
			log.Error().Msg(fmt.Sprintf("Failed to find work, got: %s", err.Error()))
			return err
		}
		if job == nil {
			m.logger.Info("No jobs to run...")
			break
		}

		err = app.ExecuteWork(m.store, job, m.BBMWorkFuncs.Work)
		if err != nil {
			log.Error().Msg(fmt.Sprintf("Failed to execute job with ID: %d for bbm with ID:%d  %s", job.ID, job.BBMID, err.Error()))
			return err
		} else {
			m.logger.Sugar().Infof("Executed work with ID for Job ID: %d for bbm with ID:%d", job.ID, job.BBMID)
		}
	}
	return nil
}

// acquireLock tries to acquire a lock on BBMs in `retryInterval` successions and tries for at least `maxFailedAttempts` times.
func acquireLock(store datastore.DB, retryInterval time.Duration, lockID, maxFailedAttempts int) bool {

	ctx := context.Background()
	var errCount int

	// Loop until the lock is acquired or timeout is reached
	for {
		// Try to acquire the lock
		err := app.Lock(ctx, store, lockID)
		if err != nil {
			errCount++
		} else {
			return true
		}

		// Check if maxFailedAttempts for acquiring lock has been reached
		if errCount >= maxFailedAttempts {
			return false
		}

		// Wait before retrying
		time.Sleep(retryInterval)
	}
}

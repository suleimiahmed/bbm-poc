package main

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/suleimiahmed/bbm-poc/internal/config"
	"gitlab.com/suleimiahmed/bbm-poc/internal/datastore"
	"gitlab.com/suleimiahmed/bbm-poc/internal/logger"
)

func init() {
	RootCmd.AddCommand(MigrateStatusCmd)
}

func main() {
	RootCmd.Execute()
}

// RootCmd is the main command for the 'registry' binary.
var RootCmd = &cobra.Command{
	Use:   "bbm-poc-cli",
	Short: "`bbm-poc-cli`",
	Long:  "`bbm-poc-cli`",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
	},
}

// MigrateStatusCmd is the `status` sub-command of `database migrate` that shows the migrations status.
var MigrateStatusCmd = &cobra.Command{
	Use:   "status",
	Short: "Show batched background migration status",
	Long:  "Show batched background migration status",
	Run: func(cmd *cobra.Command, args []string) {
		config, err := config.LoadConfig(nil)
		if err != nil {
			fmt.Fprintf(os.Stderr, "configuration error: %v\n", err)
			cmd.Usage()
			os.Exit(1)
		}

		// create a logger
		logger, err := logger.NewZapLogger()
		if err != nil {
			fmt.Fprintf(os.Stderr, "logger configuration error: %v\n", err)
			cmd.Usage()
			os.Exit(1)
		}

		_, sqldb, err := datastore.NewDB(config, logger)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to establish a databsae connection: %v\n", err)
			cmd.Usage()
			os.Exit(1)
		}

		statuses, err := Status(sqldb)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to detect database status: %v", err)
			os.Exit(1)
		}

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"Batched Background Migration", "Status"})
		table.SetColWidth(80)

		// Display table rows sorted by migration ID

		for name, status := range statuses {

			table.Append([]string{name, status.Status})
		}

		table.Render()
	},
}

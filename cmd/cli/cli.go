package main

import (
	"database/sql"
	"fmt"

	"github.com/go-gorp/gorp/v3"
	"gitlab.com/suleimiahmed/bbm-poc/app"
)

type MigrationRecord struct {
	Name   string `db:"name"`
	Status int    `db:"status"`
	ID     int    `db:"id"`
}

// migrationStatus represents the status of a migration. Unknown will be set to true if a migration is
// not known by the current build.
type migrationStatus struct {
	Status string
}

// Status returns the status of all migrations, indexed by migration name.
func Status(db *sql.DB) (map[string]*migrationStatus, error) {
	existingMigrations, err := GetMigrationRecords(db)
	if err != nil {
		return nil, err
	}

	statuses := make(map[string]*migrationStatus, len(existingMigrations))
	for _, m := range existingMigrations {
		status, ok := app.ReverseBBMstatus[m.Status]
		if !ok {
			statuses[m.Name] = &migrationStatus{Status: "unknown"}
		}
		statuses[m.Name] = &migrationStatus{Status: status}
	}

	return statuses, nil
}

func getMigrationDbMap(db *sql.DB) (*gorp.DbMap, error) {
	d := gorp.PostgresDialect{}

	// Create migration database map
	dbMap := &gorp.DbMap{Db: db, Dialect: d}

	return dbMap, nil
}

func GetMigrationRecords(db *sql.DB) ([]*MigrationRecord, error) {
	dbMap, err := getMigrationDbMap(db)
	if err != nil {
		return nil, err
	}

	var records []*MigrationRecord
	query := fmt.Sprintf("SELECT name, status, id FROM %s ORDER BY %s ASC", dbMap.Dialect.QuotedTableForQuery("public", "batched_background_migrations"), dbMap.Dialect.QuoteField("id"))
	_, err = dbMap.Select(&records, query)
	if err != nil {
		return nil, err
	}

	return records, nil
}

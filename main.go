package main

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/suleimiahmed/bbm-poc/app"
	"gitlab.com/suleimiahmed/bbm-poc/bbm"
	"gitlab.com/suleimiahmed/bbm-poc/internal/config"
	"gitlab.com/suleimiahmed/bbm-poc/internal/datastore"
	"gitlab.com/suleimiahmed/bbm-poc/internal/logger"
	"go.uber.org/zap"
)

func main() {

	// read env config
	cfg, err := config.LoadConfig(nil)
	if err != nil {
		panic(err)
	}
	// create a logger
	logger, err := logger.NewZapLogger()
	if err != nil {
		panic(err)
	}
	// create database client
	store, _, err := datastore.NewDB(cfg, logger)
	if err != nil {
		logger.Panic(err.Error())
	}

	// register work with worker
	worker, err := app.RegisterWork(bbm.AllWork()...)

	if err != nil {
		panic(err)
	}

	doneCh := make(chan struct{})

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT)
	go listenForShutdown(sigChan, doneCh)

	// blocking call to prepetually look for bbm jobs
	startScanning(worker, store, logger, cfg, doneCh)
}

// startScanning starts scanning for bbm work
func startScanning(worker *app.JobWorker, store *datastore.PgDB, logger *zap.Logger, cfg *config.Config, doneCh chan struct{}) {
	gracefulFinished, err := worker.ListenForWork(store, logger, cfg, doneCh)
	if err != nil {
		logger.Panic(err.Error())
	}
	logger.Info("bbm-poc is UP!")
	// wait for the worker to acknowledge that it is safe to exit
	<-gracefulFinished
}

func listenForShutdown(c chan os.Signal, doneCh chan struct{}) {
	<-c
	// signal to worker that the program is exiting
	close(doneCh)
}

package bbm

// bbm package contains the functions that map to the bbm `job_signature_name` in the `batched_background_migrations` table
import (
	"gitlab.com/suleimiahmed/bbm-poc/app"
)

// AllWork is the map/dictionary of all work functions the BBM process knows of, if a work function is mentioned in the `job_signature_name` column of  `batched_background_migrations`
// but it is not registered in this function then BBM process will not be able to execute the job's work.
func AllWork() []app.Work {
	return []app.Work{}
}

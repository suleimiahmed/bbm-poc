package main

import (
	"context"
	"database/sql"
	"testing"
	"time"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/suleimiahmed/bbm-poc/app"
	"gitlab.com/suleimiahmed/bbm-poc/cmd/migrate/migrations"
)

type e2eTestSuite struct {
	suite.Suite
	dbMigration *migrations.Migrator
	pgc         *PostgresContainer

	migrationDBClient *sql.DB
}

var migratorCreds = MigratorCredentials{
	DB:       "postgres",
	User:     "postgres",
	Password: "postgres",
}

func TestE2ETestSuite(t *testing.T) {
	suite.Run(t, &e2eTestSuite{})
}

func (s *e2eTestSuite) SetupSuite() {

	s.T().Log("Setting up suite...")

	s.T().Log("Setting up postgres")

	var err error
	s.pgc, err = CreatePostgresContainer(s.T(), context.Background())
	assert.NoError(s.T(), err)

	s.T().Log("Finished postgres setup")

	s.dbMigration, s.migrationDBClient, err = CreateDataMigrator(migratorCreds, s.pgc)
	assert.NoError(s.T(), err)

	s.T().Log("Starting database migration")
	if _, err := s.dbMigration.Up(); err != nil {
		s.Require().NoError(err)
	}

	s.T().Log("Finished database migration")

	InitDefaultTestEnvironmentVariables(s.T(), migratorCreds, s.pgc)

	s.T().Log("Finished suite setup!")
}

func (s *e2eTestSuite) TearDownSuite() {
}

func (s *e2eTestSuite) SetupTest() {
	s.T().Log("Starting test setup")
	s.T().Log("Setting up test")
	var err error

	// reset migrator from last test extra migrations
	s.dbMigration, _, err = CreateDataMigrator(migratorCreds, s.pgc)
	s.Require().NoError(err)

	s.T().Log("Starting migration")
	if _, err := s.dbMigration.Up(); err != nil {
		s.Require().NoError(err)
	}

	s.T().Log("Test setup complete!")
}

func (s *e2eTestSuite) TearDownTest() {
	s.T().Log("Tearing down test")
	s.T().Log("migrating down")
	if _, err := s.dbMigration.Down(); err != nil {
		s.Require().NoError(err)
	}
	s.T().Log("Test teardown complete!")
}

// Test_AsyncBBM tests that if a bbm is introduced via regular database migration, it will be picked up and executed asynchronously to completion
func (s *e2eTestSuite) Test_AsyncBBM() {
	s.T().Run("Test_AsyncBBM", func(t *testing.T) {

		cfgOverwrite := map[string]any{
			"ASYNC_BBM_JOB_INTERVAL": 1 * time.Second,
		}

		// Start the BBM process in the background
		InitializeBBMApp(t, cfgOverwrite, app.Work{Name: "CopyMediaTypesIDToNewIDColumn", Do: CopyMediaTypesIDToNewIDColumn})

		// Create migrator
		dbMigration, dbClient, err := CreateDataMigrator(migratorCreds, s.pgc, app.Work{Name: "CopyMediaTypesIDToNewIDColumn", Do: CopyMediaTypesIDToNewIDColumn})
		s.Require().NoError(err)

		// Add a new regular migration to introduce a new column
		dbMigration.InjectMigration([]*migrations.Migration{{
			Migration: &migrate.Migration{
				Id: time.Now().Add(-1*time.Minute).Format("20060102150405") + "_create_a_new_column_in_media_types_table",
				Up: []string{
					`ALTER TABLE media_types ADD COLUMN IF NOT EXISTS new_id bigint`,
				},
				Down: []string{
					"ALTER TABLE media_types DROP COLUMN IF EXISTS new_id",
				},
			},
		}})

		// Add a new regular migration to introduce a BBM to copy from one column in the table to another.
		dbMigration.InjectMigration([]*migrations.Migration{{
			Migration: &migrate.Migration{
				Id: time.Now().Format("20060102150405") + "_bbm_to_copy_from_media_types_id_column_to_new_column",
				Up: []string{`INSERT INTO batched_background_migrations ("name", "created_at", "updated_at", "min_value", "max_value", "batch_size", "status", "job_signature_name", "table_name", "column_name")
			VALUES ('CopyMediaTypesIDToNewIDColumn', NOW(), NOW(), 1, (SELECT MAX("id") FROM media_types),  20, 1, 'CopyMediaTypesIDToNewIDColumn', 'media_types', 'id')`},
				Down: []string{
					`DELETE FROM batched_background_migration_jobs WHERE batched_background_migration_id IN (SELECT id FROM batched_background_migrations WHERE name = 'CopyMediaTypesIDToNewIDColumn')`,
					`DELETE FROM batched_background_migrations WHERE "name" = 'CopyMediaTypesIDToNewIDColumn'`,
				},
			},
		}})

		_, err = dbMigration.Up()
		assert.NoError(t, err)
		t.Cleanup(func() {
			_, err := dbMigration.Down()
			assert.NoError(t, err)
		})

		//assert the BBM runs to completion asynchronously after 30 seconds
		assert.Eventually(t,
			func() bool {
				rows, err := dbClient.Query(`SELECT status FROM batched_background_migrations WHERE "name" = 'CopyMediaTypesIDToNewIDColumn' AND "status" = 2`)
				if err != nil {
					t.Log(err)
					return false
				}

				var status *int
				defer rows.Close()
				for rows.Next() {
					err = rows.Scan(&status)
					if err != nil {
						t.Log(err)
						return false
					} else {
						return true
					}
				}
				return false
			}, 30*time.Second, 2*time.Second)
	})
}

// Test_SyncBBM tests that when a bbm is introduced via regular database migration and is refrenced as a dependency
// for conitnuing with a following regular database mirations, it will be picked up and executed on the spot before other regular database migrations can continue.
func (s *e2eTestSuite) Test_SyncBBM() {
	s.T().Run("Test_SyncBBM", func(t *testing.T) {
		// Create migrator
		dbMigration, dbClient, err := CreateDataMigrator(migratorCreds, s.pgc, app.Work{Name: "CopyMediaTypesIDToNewIDColumn", Do: CopyMediaTypesIDToNewIDColumn})
		s.Require().NoError(err)

		// Add a new regular migration to introduce a new column
		dbMigration.InjectMigration([]*migrations.Migration{{
			Migration: &migrate.Migration{
				Id: time.Now().Add(-2*time.Minute).Format("20060102150405") + "_create_a_new_column_in_media_types_table",
				Up: []string{
					`ALTER TABLE media_types ADD COLUMN IF NOT EXISTS new_id bigint`,
				},
				Down: []string{
					"ALTER TABLE media_types DROP COLUMN IF EXISTS new_id",
				},
			},
		}})

		// Add a new regular migration to introduce a BBM to copy from one column in the table to another.
		dbMigration.InjectMigration([]*migrations.Migration{{
			Migration: &migrate.Migration{
				Id: time.Now().Add(-1*time.Minute).Format("20060102150405") + "_bbm_to_copy_from_media_types_id_column_to_new_column",
				Up: []string{`INSERT INTO batched_background_migrations ("name", "created_at", "updated_at", "min_value", "max_value", "batch_size", "status", "job_signature_name", "table_name", "column_name")
			VALUES ('CopyMediaTypesIDToNewIDColumn', NOW(), NOW(), 1, (SELECT MAX("id") FROM media_types),  20, 1, 'CopyMediaTypesIDToNewIDColumn', 'media_types', 'id')`},
				Down: []string{
					`DELETE FROM batched_background_migration_jobs WHERE batched_background_migration_id IN (SELECT id FROM batched_background_migrations WHERE name = 'CopyMediaTypesIDToNewIDColumn')`,
					`DELETE FROM batched_background_migrations WHERE "name" = 'CopyMediaTypesIDToNewIDColumn'`,
				},
			},
		}})

		// Add a new regular migration to that asserts that the BBM introduced earlier is complete.
		// This should force the BBM to complete.
		dbMigration.InjectMigration([]*migrations.Migration{{
			Migration: &migrate.Migration{
				Id: time.Now().Format("20060102150405") + "_create_a_dummy_table",
				Up: []string{`CREATE TABLE dummy_table (
				id bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY,
				column_a VARCHAR(255) NOT NULL,
				column_b VARCHAR(255) NOT NULL,
				column_c VARCHAR(255) NOT NULL,
				column_d VARCHAR(255) NOT NULL,
				CONSTRAINT pk PRIMARY KEY (id)
			)`},
				Down: []string{
					`DROP TABLE IF EXISTS dummy_table;`,
				},
			},
			EnsureBBMBefore: []string{"CopyMediaTypesIDToNewIDColumn"},
		}})

		_, err = dbMigration.Up()
		assert.NoError(t, err)
		t.Cleanup(func() {
			_, err := dbMigration.Down()
			assert.NoError(t, err)
		})

		//assert the BBM ran to completion synchronously after the regular database migration
		rows, err := dbClient.Query(`SELECT status FROM batched_background_migrations WHERE "name" = 'CopyMediaTypesIDToNewIDColumn' AND "status" = 2`)
		assert.NoError(t, err)

		var status *int
		defer rows.Close()
		for rows.Next() {
			err = rows.Scan(&status)
			assert.NoError(t, err)
		}
		assert.Equal(t, 2, *status)
	})
}

package main

import (
	"context"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/suleimiahmed/bbm-poc/internal/datastore"
)

// CopyMediaTypesIDToNewIDColumn is the unit of work that is executed for a BBM with a `job_signature_name` column value of `CopyMediaTypesIDToNewIDColumn`
func CopyMediaTypesIDToNewIDColumn(ctx context.Context, store datastore.DB, paginationColumn string, paginationAfter, paginationBefore, limit int) error {
	fmt.Printf(`Copying from column id to new_id,  Starting from id %d to %d on column %s,  taking only the first %d values.`, paginationAfter, paginationBefore, paginationColumn, limit)

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	q := psql.Update("media_types").
		Set("new_id", sq.Expr("id")).
		Where(sq.And{sq.GtOrEq{paginationColumn: paginationAfter}, sq.LtOrEq{paginationColumn: paginationBefore}})

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}
	_, err = store.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}
	return nil
}

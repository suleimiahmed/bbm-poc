# BBM-POC

A proof of concept implementation of GitLab's container registry Batched Background Migration proposal explained in: https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md 

An example of creating a BBM to run asynchronously (or synchronously - as part of regular database migration) can be found in:
- https://gitlab.com/suleimiahmed/bbm-poc/-/merge_requests/3
- https://gitlab.com/suleimiahmed/bbm-poc/-/merge_requests/2

To see the BBM run in action, clone this repo, git-checkout any of the branches in the MR above and `docker-compose up` in the root of the project. 

This will:
- Start a postgres container.
- Run regular database migrations against the postgres container.
- Start the BBM process to run available async/sync BBMs (depending on the branch checked out).

To check the status of a BBM:
- create a shell in the running BBM process (e.g. `docker exec -it bbm-poc sh`)
- run `bbm-poc-cli status`
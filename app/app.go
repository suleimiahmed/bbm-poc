package app

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"math/rand"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/rs/zerolog/log"
	"gitlab.com/suleimiahmed/bbm-poc/internal/config"
	"gitlab.com/suleimiahmed/bbm-poc/internal/datastore"
	"go.uber.org/zap"
)

const (
	// LockID is theb BBM Lock reference.
	LockID = 0
	// BBM and BBM job statuses.
	ActiveStatus   = "active"
	RunningStatus  = "running"
	FailedStatus   = "failed"
	FinishedStatus = "finished"
	PausedStatus   = "paused"
)

var (
	// ErrlockInUse is  returned when a BBM job worker can not retreive the distributed lock because it has not been released by another worker.
	ErrlockInUse = errors.New("database mutex is already taken")
)

// BBMstatus are the BBM and BBM job statuses as defined in:
// https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md#batch-background-migration-bbm-creation
var BBMstatus = map[string]int{
	PausedStatus:   0,
	ActiveStatus:   1,
	FinishedStatus: 2,
	FailedStatus:   3,
	RunningStatus:  4,
}

// ReverseBBMstatus are the (reversed) BBM and BBM job statuses as defined in:
// https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/gitlab/database-background-migrations.md#batch-background-migration-bbm-creation
var ReverseBBMstatus = map[int]string{
	0: PausedStatus,
	1: ActiveStatus,
	2: FinishedStatus,
	3: FailedStatus,
	4: RunningStatus,
}

// BBM is the representation of a BBM.
type BBM struct {
	ID           int
	Status       int
	StartID      int
	EndID        int
	BatchSize    int
	JobName      string
	TargetTable  string
	TargetColumn string
}

// Job is the representation of a BBM Job.
type Job struct {
	ID               int
	BBMID            int
	StartID          int
	EndID            int
	Status           int
	Attempts         int
	JobName          string
	PaginationColumn string
	BatchSize        int
}

// Work represents the unit of work that a BBM job is capable of executing at a time.
type Work struct {
	// Name must correspond to the `job_signature_name` in `batched_background_migrations` table
	Name string
	// Do is the work function that is assigned to the job
	Do func(ctx context.Context, db datastore.DB, paginationColumn string, paginationAfter, paginationBefore, limit int) error
}

// RegisterWork is used to register all known work functions and to the the BBM process.
// This is what maps the `job_signature_name` column values in the BBM table to actual in-code functions.
// In order to make sure a BBM added to the BBM table is executed, you must make sure the `job_signature_name` has been mapped to an actual work struct that is passed to `RegisterWork`.
func RegisterWork(work ...Work) (*JobWorker, error) {
	workMap := map[string]Work{}
	for _, val := range work {
		if _, found := workMap[val.Name]; found {
			return nil, fmt.Errorf("can not have work with the same name %s", val.Name)
		}
		workMap[val.Name] = val
	}
	return &JobWorker{workMap}, nil
}

// Worker is an entity that must be able to listen for work whenever necessary and try to pick the work up.
type Worker interface {
	ListenForWork(store datastore.DB, logger *zap.Logger, cfg *config.Config, doneChan <-chan struct{}) (chan struct{}, error)
}

// JobWorker is an implementation of Worker interface.
type JobWorker struct {
	Work map[string]Work
}

// ListenForWork allows a JobWorker to inspect the database for jobs that need to be executed and if applicable execute the work unit.
// The inspection of the database for potential work is carried out at a period of `cfg.BBM.JobInterval` set in the configuration.
// Once a job is found the worker attempts to obtain a distributed lock and execute the job to completion.
// However, if the lock is held by aother worker in a seprate process it waits another `cfg.BBM.JobInterval` duration and tries again.
func (jw *JobWorker) ListenForWork(store datastore.DB, logger *zap.Logger, cfg *config.Config, doneChan <-chan struct{}) (chan struct{}, error) {
	// gracefullFinish is used to signal to an upstream processes that a worker has completed any in-flight jobs and the upstream can terminate if needed.
	gracefullFinish := make(chan struct{})
	// Create a period that this worker searches and executes work (use a random jitter of cfg.AsyncBBM.JobIntervalJitter seconds for obscurity)
	ticker := time.NewTicker(cfg.AsyncBBM.JobInterval + time.Duration(rand.Intn(int(cfg.AsyncBBM.JobIntervalJitter)))*time.Second)

	go func() {
	out:
		for {
			logger.Info("Waiting for next cycle...")
			select {
			// The upstream process is terminating, this worker should exit.
			case <-doneChan:
				//cleanup
				logger.Info("Received shutdown signal: Shutting down...")
				close(gracefullFinish)
				break out
			// A period has elapsed, time to try to find and execute any availaible jobs.
			case <-ticker.C:
				logger.Info("Searching for Work...")
				job, err := FindWork(store, logger, cfg.AsyncBBM.MaxJobAttempts)
				if err != nil {
					if errors.Is(err, ErrlockInUse) {
						logger.Sugar().Infof("Could not fetch job, another job is running on a different process: %w", ErrlockInUse)
					}
					log.Error().Msg(fmt.Sprintf("Failed to find work, got: %s", err.Error()))
					break
				}
				if job == nil {
					logger.Info("No jobs to run...")
					break
				}

				// A job was found, lets execute it
				err = ExecuteWork(store, job, jw.Work)
				if err != nil {
					log.Error().Msg(fmt.Sprintf("Failed to execute job with ID: %d for bbm with ID:%d  %s", job.ID, job.BBMID, err.Error()))
				} else {
					logger.Sugar().Infof("Executed work with ID for Job ID: %d for bbm with ID:%d", job.ID, job.BBMID)
				}

			}
		}
	}()

	return gracefullFinish, nil

}

// FindWork checks for any BBM work that needs to be run
// if work needs to be done it either fetches the job or creates the job
// associated with the unit of work.
func FindWork(store datastore.DB, logger *zap.Logger, maxJobAttempts int) (*Job, error) {

	ctx := context.Background()

	// Find a bbm that needs to be run
	bbm, err := FindFirstActiveOrRunningBBM(store, ctx)
	if err != nil {
		return nil, err
	}
	if bbm == nil {
		return nil, nil
	}

	// If a bbm was found, acquire a lock so no other migrations can be run
	err = Lock(ctx, store, LockID)
	if err != nil {
		return nil, err
	}
	defer logger.Info("Released lock")
	defer Unlock(ctx, store, LockID)

	logger.Info("Obtained lock")

	var (
		start int
		last  = bbm.EndID
	)

	// Check if any jobs for the bbm exist with the end bound of the bbm.
	// This signifies we've run all jobs of the bbm at least once.
	finalJob, err := FindBBMJobWithEndID(ctx, store, bbm.ID, last)
	if err != nil {
		return nil, err
	}

	// we havent run all jobs for the bbm at leas once.
	if finalJob == nil {
		// find the last job that was created for the BBM
		lastCreatedJob, err := FindLastJobForBBM(ctx, store, bbm)
		if err != nil {
			return nil, err
		}
		// if the bbm does not have any job, this implies it has never been run/started, so start it!
		if lastCreatedJob == nil {
			start = bbm.StartID
			err = UpdateBBMStatus(ctx, store, bbm.ID, BBMstatus[RunningStatus])
			if err != nil {
				return nil, err
			}
		} else {
			// otherwise find the starting point for the next job that should be created for the BBM.
			start = lastCreatedJob.EndID + 1
			// the start point of the job to be created must not be greater than the BBM end bound.
			if start > last {
				return nil, nil
			}
		}

		// Based on the bbm batch side and the start point of the job find the endpoint of the job's end point.
		foundEnd, err := FindJobEndFromJobStart(ctx, store, bbm.TargetTable, bbm.TargetColumn, start, last, bbm.BatchSize)
		if err != nil {
			return nil, err
		}
		if foundEnd == nil {
			return nil, errors.New("could not find the end of job")
		}
		end := *foundEnd

		// create the job
		newJob := &Job{
			BBMID:     bbm.ID,
			StartID:   start,
			EndID:     end,
			BatchSize: bbm.BatchSize,
			Status:    BBMstatus[ActiveStatus],
		}
		job, err := CreateNewJob(ctx, store, newJob)
		if err != nil {
			return nil, err
		}
		// decorate job with bbm attributes
		job.JobName = bbm.JobName
		job.PaginationColumn = bbm.TargetColumn
		job.BatchSize = bbm.BatchSize

		return job, nil
	} else {
		// We've created and run every possible job for the current migration at least once.
		// Now we need to check that non of the jobs need to be retried.
		job, err := FindBBMJobWithStatus(ctx, store, bbm.ID, BBMstatus[FailedStatus])
		if err != nil {
			return nil, err
		}
		// if there are no jobs that failed update the migration to finished state
		if job == nil {
			err = UpdateBBMStatus(ctx, store, bbm.ID, BBMstatus[FinishedStatus])
			if err != nil {
				return nil, err
			}
			return nil, err
		}
		// decorate job with bbm attributes
		job.JobName = bbm.JobName
		job.PaginationColumn = bbm.TargetColumn
		job.BatchSize = bbm.BatchSize

		// otherwise check that the returned job does not exceed the MaxJobAttempt
		if job.Attempts >= maxJobAttempts {
			logger.Sugar().Infof("maximum job attempt reached for job id: %d", job.ID)
			// mark BBM as failed
			logger.Sugar().Infof("Marked bbm id %s as failed", bbm.ID)
			err = UpdateBBMStatus(ctx, store, bbm.ID, BBMstatus[FailedStatus])
			if err != nil {
				return nil, err
			}
			return nil, err
		}
		// if the job does not exceed the MaxJobAttempt it should be retried.
		logger.Sugar().Infof("qeueing job id: %d for execution on attempt: %d", job.ID, job.Attempts)
		return job, nil

	}
}

func FindExecutableJobForBBM(store datastore.DB, logger *zap.Logger, name string, maxJobAttempts int) (*Job, error) {

	ctx := context.Background()

	// find a migration that needs to be run
	bbm, err := FindBBMWithName(store, ctx, name)
	if err != nil {
		return nil, err
	}
	if bbm == nil {
		return nil, nil
	}

	var start int
	last := bbm.EndID

	// Check if any jobs exist with the end bound of the bbm
	finalJob, err := FindBBMJobWithEndID(ctx, store, bbm.ID, last)
	if err != nil {
		return nil, err
	}

	// we havent created all jobs
	if finalJob == nil {
		lastCreatedJob, err := FindLastJobForBBM(ctx, store, bbm)
		if err != nil {
			return nil, err
		}

		if lastCreatedJob == nil {
			start = bbm.StartID
			err = UpdateBBMStatus(ctx, store, bbm.ID, BBMstatus[RunningStatus])
			if err != nil {
				return nil, err
			}
		} else {
			start = lastCreatedJob.EndID + 1
			if start > last {
				return nil, nil
			}
		}

		foundEnd, err := FindJobEndFromJobStart(ctx, store, bbm.TargetTable, bbm.TargetColumn, start, last, bbm.BatchSize)
		if err != nil {
			return nil, err
		}
		if foundEnd == nil {
			return nil, errors.New("could not find the end of job")
		}
		end := *foundEnd

		newJob := &Job{
			BBMID:     bbm.ID,
			StartID:   start,
			EndID:     end,
			BatchSize: bbm.BatchSize,
			Status:    BBMstatus[ActiveStatus],
		}
		job, err := CreateNewJob(ctx, store, newJob)
		if err != nil {
			return nil, err
		}

		// decorate job with bbm attributes
		job.JobName = bbm.JobName
		job.PaginationColumn = bbm.TargetColumn
		job.BatchSize = bbm.BatchSize

		return job, nil
	} else {
		// we've created every possible job for the current migration
		// now we need to check that non of the jobs need to be retried

		job, err := FindBBMJobWithStatus(ctx, store, bbm.ID, BBMstatus[FailedStatus])
		if err != nil {
			return nil, err
		}
		// if there are no jobs that failed update the migration to finished
		if job == nil {
			err = UpdateBBMStatus(ctx, store, bbm.ID, BBMstatus[FinishedStatus])
			if err != nil {
				return nil, err
			}
			return nil, err
		}
		// decorate job with bbm attributes
		job.JobName = bbm.JobName
		job.PaginationColumn = bbm.TargetColumn
		job.BatchSize = bbm.BatchSize

		// otherwise check that the returned job does not exceed the MaxJobAttempt
		if job.Attempts >= maxJobAttempts {
			logger.Sugar().Infof("maximum job attempt reached for job id: %d", job.ID)
			// mark BBM as failed
			logger.Sugar().Infof("Marked bbm id %s as failed", bbm.ID)
			err = UpdateBBMStatus(ctx, store, bbm.ID, BBMstatus[FailedStatus])
			if err != nil {
				return nil, err
			}
			return nil, err
		}
		// otherwise try to rerun the job
		logger.Sugar().Infof("qeueing job id: %d for execution on attempt: %d", job.ID, job.Attempts)
		return job, nil

	}
}

// CreateNewJob creates a new job entry in the `batched_background_migration_jobs` table.
func CreateNewJob(ctx context.Context, store datastore.DB, newJob *Job) (*Job, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	query := psql.
		Insert("batched_background_migration_jobs").Columns("created_at", "updated_at", "batched_background_migration_id", "min_value", "max_value", "status").
		Values(time.Now(), time.Now(), newJob.BBMID, newJob.StartID, newJob.EndID, newJob.Status).
		Suffix(`RETURNING "id", "batched_background_migration_id", "min_value", "max_value", "status", "attempts"`)

	sql, args, err := query.ToSql()
	if err != nil {
		return nil, err
	}
	result, err := store.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, err
	}
	defer result.Close()

	var (
		id, bbmID, startID, endID, status, attempts int
	)
	result.Next()
	err = result.Scan(&id, &bbmID, &startID, &endID, &status, &attempts)
	if err != nil {
		return nil, err
	}
	newJob.ID = id
	newJob.BBMID = bbmID
	newJob.StartID = startID
	newJob.EndID = endID
	newJob.Status = status
	newJob.Attempts = attempts

	return newJob, nil
}

// FindJobEndFromJobStart finds the end cursor for a job based on the start of the job and the batch size of the bbm the job is associated with.
func FindJobEndFromJobStart(ctx context.Context, store datastore.DB, table string, column string, start, last, batchSize int) (*int, error) {
	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select(column).From(table).Where(sq.And{sq.GtOrEq{column: start}, sq.LtOrEq{column: last}}).OrderBy(column).Limit(uint64(batchSize))

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return nil, err
	}

	var end *int
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&end)
		if err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, err
		}
	}
	return end, nil
}

// FindLastJobForBBM returns the last job created for a BBM.
func FindLastJobForBBM(ctx context.Context, store datastore.DB, bbm *BBM) (*Job, error) {
	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("id", "batched_background_migration_id", "min_value", "max_value", "status", "attempts").
		From("batched_background_migration_jobs").Where(sq.Eq{"batched_background_migration_id": bbm.ID}).OrderBy("id DESC").Limit(1)

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return nil, err
	}

	var job *Job
	defer rows.Close()
	for rows.Next() {
		job = &Job{}
		err = rows.Scan(&job.ID, &job.BBMID, &job.StartID, &job.EndID, &job.Status, &job.Attempts)
		if err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, err
		}
	}
	return job, nil
}

// ExecuteWork attempts to execute the work function associated with a BBM for the job start-end range.
func ExecuteWork(store datastore.DB, job *Job, registeredWork map[string]Work) error {
	ctx := context.Background()
	// update the job attempts
	err := UpdateBBMJobAttempts(ctx, store, job.ID, job.Attempts+1)
	if err != nil {
		return err
	}
	if jobWork, found := registeredWork[job.JobName]; found {
		err := jobWork.Do(ctx, store, job.PaginationColumn, job.StartID, job.EndID, job.BatchSize)
		if err != nil {
			// mark job as failed
			err = UpdateBBMJobStatus(ctx, store, job.ID, BBMstatus[FailedStatus])
			if err != nil {
				return err
			}
			return fmt.Errorf("failed executing job for id %d: %w", job.ID, err)
		} else {
			// mark job as finished
			return UpdateBBMJobStatus(ctx, store, job.ID, BBMstatus[FinishedStatus])
		}

	} else {
		// mark job as failed
		err := UpdateBBMJobStatus(ctx, store, job.ID, BBMstatus[FailedStatus])
		if err != nil {
			return err
		}
		return fmt.Errorf("job signature not found for job with ID %d", job.ID)
	}

}

// Lock sets a lock for a migration job run.
func Lock(ctx context.Context, store datastore.DB, lockID int) error {
	var result bool
	err := store.QueryRowContext(ctx, fmt.Sprintf("SELECT pg_try_advisory_lock(%d)", lockID)).Scan(&result)
	if err != nil {
		return err
	}
	if !result {
		return ErrlockInUse
	}
	return nil
}

// Unlock releases a lock for a migration job run.
func Unlock(ctx context.Context, store datastore.DB, lockID int) error {
	var result bool
	err := store.QueryRowContext(ctx, fmt.Sprintf("SELECT pg_advisory_unlock(%d)", lockID)).Scan(&result)
	if err != nil {
		return err
	}
	if !result {
		return errors.New("unable to release database mutex")
	}
	return nil
}

// FindFirstActiveOrRunningBBM finds the first active or running BBM by ascending order on the BBM `id“ column
func FindFirstActiveOrRunningBBM(store datastore.DB, ctx context.Context) (*BBM, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("id", "min_value", "max_value", "batch_size", "status", "job_signature_name", "table_name", "column_name").
		From("batched_background_migrations").Where(sq.Or{sq.Eq{"status": BBMstatus[ActiveStatus]}, sq.Eq{"status": BBMstatus[RunningStatus]}}).OrderBy("id ASC").Limit(1)

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return nil, err
	}

	var bbm *BBM
	defer rows.Close()
	for rows.Next() {
		bbm = &BBM{}
		err = rows.Scan(&bbm.ID, &bbm.StartID, &bbm.EndID, &bbm.BatchSize, &bbm.Status, &bbm.JobName, &bbm.TargetTable, &bbm.TargetColumn)
		if err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, err
		}
	}
	return bbm, nil
}

// FindBBMJobWithEndID returns any jobs with the end id `endID`
func FindBBMJobWithEndID(ctx context.Context, store datastore.DB, bbmID, endID int) (*Job, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("id", "batched_background_migration_id", "min_value", "max_value", "status", "attempts").
		From("batched_background_migration_jobs").Where(sq.And{sq.Eq{"batched_background_migration_id": bbmID}, sq.Eq{"max_value": endID}})

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return nil, err
	}

	var job *Job
	defer rows.Close()
	for rows.Next() {
		job = &Job{}
		err = rows.Scan(&job.ID, &job.BBMID, &job.StartID, &job.EndID, &job.Status, &job.Attempts)
		if err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, err
		}
	}
	return job, nil
}

// FindBBMJobWithStatus returns any jobs with the status `status`
func FindBBMJobWithStatus(ctx context.Context, store datastore.DB, bbmID, status int) (*Job, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("id", "batched_background_migration_id", "min_value", "max_value", "status", "attempts").
		From("batched_background_migration_jobs").Where(sq.And{sq.Eq{"batched_background_migration_id": bbmID}, sq.Eq{"status": status}})

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return nil, err
	}

	var job *Job
	defer rows.Close()
	for rows.Next() {
		job = &Job{}
		err = rows.Scan(&job.ID, &job.BBMID, &job.StartID, &job.EndID, &job.Status, &job.Attempts)
		if err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, err
		}
	}
	return job, nil
}

// UpdateBBMStatus updates the status of a BBM to `status`
func UpdateBBMStatus(ctx context.Context, store datastore.DB, bbmID, status int) error {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	q := psql.Update("batched_background_migrations").
		Set("status", status).Set("updated_at", time.Now()).
		Where(sq.Eq{"id": bbmID})

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}
	_, err = store.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}
	return nil
}

// UpdateBBMStatus updates the status of a BBM job to `status`
func UpdateBBMJobStatus(ctx context.Context, store datastore.DB, bbmJobID, status int) error {
	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	q := psql.Update("batched_background_migration_jobs").Where(sq.Eq{"id": bbmJobID}).
		Set("status", status).Set("updated_at", time.Now())

	if status == BBMstatus[FinishedStatus] {
		q = q.Set("finished_at", time.Now())
	}

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}
	_, err = store.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}
	return nil
}

// UpdateBBMJobAttempts updates the nu,ber of attempts of a BBM job to `attempts`
func UpdateBBMJobAttempts(ctx context.Context, store datastore.DB, bbmJobID, attempts int) error {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	q := psql.Update("batched_background_migration_jobs").
		Set("attempts", attempts).Set("updated_at", time.Now()).
		Set("started_at", time.Now()).
		Where(sq.Eq{"id": bbmJobID})

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}
	_, err = store.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}
	return nil
}

// FindBBMWithId find a BBM with id `id`
func FindBBMWithId(store datastore.DB, ctx context.Context, id int) (*BBM, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("id", "min_value", "max_value", "batch_size", "status", "job_signature_name", "table_name", "column_name").
		From("batched_background_migrations").Where(sq.Eq{"id": id})

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return nil, err
	}

	var bbm *BBM
	defer rows.Close()
	for rows.Next() {
		bbm = &BBM{}
		err = rows.Scan(&bbm.ID, &bbm.StartID, &bbm.EndID, &bbm.BatchSize, &bbm.Status, &bbm.JobName, &bbm.TargetTable, &bbm.TargetColumn)
		if err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, err
		}
	}
	return bbm, nil
}

// FindBBMWithName find a BBM with name `name`
func FindBBMWithName(store datastore.DB, ctx context.Context, name string) (*BBM, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("id", "min_value", "max_value", "batch_size", "status", "job_signature_name", "table_name", "column_name").
		From("batched_background_migrations").Where(sq.Eq{"name": name})

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return nil, err
	}

	var bbm *BBM
	defer rows.Close()
	for rows.Next() {
		bbm = &BBM{}
		err = rows.Scan(&bbm.ID, &bbm.StartID, &bbm.EndID, &bbm.BatchSize, &bbm.Status, &bbm.JobName, &bbm.TargetTable, &bbm.TargetColumn)
		if err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, err
		}
	}
	return bbm, nil
}

// IsBBMCompleteByID Checks that a BBM is complete based on the bbm id.
func IsBBMCompleteByID(ctx context.Context, store datastore.DB, bbmID int) (bool, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("status").
		From("batched_background_migrations").Where(sq.Eq{"id": bbmID})

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return false, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return false, err
	}

	var status int
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&status)
		if err != nil {
			return false, err
		}
	}
	if status != 2 {
		return false, nil
	}

	return true, nil
}

// IsBBMCompleteByName Checks that a BBM is complete based on the bbm name.
func IsBBMCompleteByName(ctx context.Context, store datastore.DB, bbmName string) (bool, error) {

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	userQuery := psql.Select("status").
		From("batched_background_migrations").Where(sq.Eq{"name": bbmName})

	sqlStmt, args, err := userQuery.ToSql()
	if err != nil {
		return false, err
	}
	rows, err := store.QueryContext(ctx, sqlStmt, args...)
	if err != nil {
		return false, err
	}

	var status int
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&status)
		if err != nil {
			return false, err
		}
	}
	if status != 2 {
		return false, nil
	}

	return true, nil
}

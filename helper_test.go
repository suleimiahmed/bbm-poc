package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"testing"
	"time"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/rs/zerolog"
	sqldblogger "github.com/simukti/sqldb-logger"
	"github.com/simukti/sqldb-logger/logadapter/zerologadapter"
	"github.com/stretchr/testify/assert"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/postgres"
	"github.com/testcontainers/testcontainers-go/wait"
	"gitlab.com/suleimiahmed/bbm-poc/app"
	"gitlab.com/suleimiahmed/bbm-poc/bbm"
	"gitlab.com/suleimiahmed/bbm-poc/cmd/migrate/migrations"
	"gitlab.com/suleimiahmed/bbm-poc/internal/config"
	"gitlab.com/suleimiahmed/bbm-poc/internal/datastore"
	"gitlab.com/suleimiahmed/bbm-poc/internal/logger"
)

type PostgresContainer struct {
	*postgres.PostgresContainer
	ConnectionString string
	Host             string
	Port             string
	ContainerIP      string
}
type PostgresContainerConfig struct {
	MigratorCredentials
}

func CreatePostgresContainer(t *testing.T, ctx context.Context) (*PostgresContainer, error) {
	log.Println("Initializing postgres container and scripts")
	pgContainer, err := postgres.RunContainer(ctx,
		testcontainers.WithImage("postgres:15.3-alpine"),
		postgres.WithDatabase("postgres"),
		postgres.WithUsername("postgres"),
		postgres.WithPassword("postgres"),
		testcontainers.WithWaitStrategy(
			wait.ForLog("database system is ready to accept connections").
				WithOccurrence(2).WithStartupTimeout(5*time.Second)),
	)
	if err != nil {
		return nil, err
	}
	connStr, err := pgContainer.ConnectionString(ctx, "sslmode=disable")
	if err != nil {
		return nil, err
	}

	containerIP, err := pgContainer.ContainerIP(ctx)
	if err != nil {
		return nil, err
	}

	ip, err := pgContainer.Host(ctx)
	if err != nil {
		return nil, err
	}
	port, err := pgContainer.MappedPort(ctx, "5432")
	if err != nil {
		return nil, err
	}

	return &PostgresContainer{
		PostgresContainer: pgContainer,
		ConnectionString:  connStr,
		Host:              ip,
		Port:              port.Port(),
		ContainerIP:       containerIP,
	}, nil
}

func GetInitScript(t *testing.T) []string {
	var initScripts []string
	pwd, err := os.Getwd()
	assert.NoError(t, err)
	path := filepath.Join(pwd, "..", "scripts", "initdb")
	files, err := os.ReadDir(path)
	assert.NoError(t, err)
	for _, file := range files {
		initScripts = append(initScripts, filepath.Join(path, file.Name()))
	}
	return initScripts

}

type MigratorCredentials struct {
	DB       string
	User     string
	Password string
}

func CreateDataMigrator(cfg MigratorCredentials, pgc *PostgresContainer, additionalBBMworkFunc ...app.Work) (*migrations.Migrator, *sql.DB, error) {

	configOverwrite := map[string]any{
		"DB_HOST":     pgc.Host,
		"DB_PORT":     pgc.Port,
		"DB_NAME":     cfg.DB,
		"DB_USER":     cfg.User,
		"DB_PASSWORD": cfg.Password,
	}

	appCfg, err := config.LoadConfig(configOverwrite)
	if err != nil {
		return nil, nil, err
	}

	// create a logger
	logger, err := logger.NewZapLogger()
	if err != nil {
		return nil, nil, err
	}

	// create database client
	store, sqldb, err := datastore.NewDB(appCfg, logger)
	if err != nil {
		return nil, nil, err
	}

	// register work with worker
	worker, err := app.RegisterWork(
		append(bbm.AllWork(), additionalBBMworkFunc...)...,
	)
	if err != nil {
		return nil, nil, err
	}

	mig := migrations.NewMigrator(store, sqldb, logger, worker, appCfg.SyncBBM)
	_, err = mig.Up()
	if err != nil {
		return nil, nil, err
	}

	return mig, sqldb, err
}

func NewDB(dbHost, dbPort, dbUser, dbPassword, dbName string) (*sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		dbHost, dbPort, dbUser, dbPassword, dbName)

	sqldb, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	loggerAdapter := zerologadapter.New(zerolog.New(os.Stdout))
	sqldb = sqldblogger.OpenDriver(psqlInfo, sqldb.Driver(), loggerAdapter)
	err = sqldb.Ping()
	if err != nil {
		return nil, err
	}
	return sqldb, nil
}

func InitDefaultTestEnvironmentVariables(t *testing.T, mig MigratorCredentials, postgres *PostgresContainer) {
	t.Setenv("REQUEST_TIMEOUT", "10s")
	t.Setenv("DB_HOST", postgres.Host)
	t.Setenv("DB_PORT", postgres.Port)
	t.Setenv("DB_USER", mig.User)
	t.Setenv("DB_PASSWORD", mig.Password)
	t.Setenv("DB_NAME", mig.DB)
}

func InitializeBBMApp(t *testing.T, configOverwrite map[string]any, additionalBBMworkFunc ...app.Work) error {

	appCfg, err := config.LoadConfig(configOverwrite)
	if err != nil {
		return err
	}

	// create a logger
	logger, err := logger.NewZapLogger()
	if err != nil {
		return err
	}

	// create database client
	store, _, err := datastore.NewDB(appCfg, logger)
	if err != nil {
		return err
	}

	// register work with worker
	worker, err := app.RegisterWork(
		append(bbm.AllWork(), additionalBBMworkFunc...)...,
	)
	if err != nil {
		return err
	}

	doneCh := make(chan struct{})
	t.Cleanup(func() {
		close(doneCh)
	},
	)

	go startScanning(worker, store, logger, appCfg, doneCh)

	return nil
}

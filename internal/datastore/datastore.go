package datastore

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os"

	"github.com/rs/zerolog"
	sqldblogger "github.com/simukti/sqldb-logger"
	"github.com/simukti/sqldb-logger/logadapter/zerologadapter"
	"gitlab.com/suleimiahmed/bbm-poc/internal/config"
	"go.uber.org/zap"

	_ "github.com/lib/pq"
)

type DB interface {
	Ping() error
	QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	Commit() error
	Rollback() error
	Begin() (*sql.Tx, error)
}

type db struct {
	*sql.DB
}

func (db *db) Commit() error {
	return nil
}
func (db *db) Rollback() error {
	return errors.New("can not rollback when using a non-transactional db")
}

type tx struct {
	*sql.Tx
	db DB
}

func (tx *tx) Begin() (*sql.Tx, error) {
	return tx.db.Begin()
}

func (tx *tx) Ping() error {
	return tx.db.Ping()
}

type PgDB struct {
	DB
}

func NewDB(cfg *config.Config, log *zap.Logger) (*PgDB, *sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		cfg.DBHost, cfg.DBPort, cfg.DBUser, cfg.DBPassword, cfg.DBName)

	sqldb, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, nil, err
	}
	if cfg.DebugMode {
		loggerAdapter := zerologadapter.New(zerolog.New(os.Stdout))
		sqldb = sqldblogger.OpenDriver(psqlInfo, sqldb.Driver(), loggerAdapter)
	}
	err = sqldb.Ping()
	if err != nil {
		return nil, nil, err
	}
	return &PgDB{&db{sqldb}}, sqldb, err
}

package logger

import (
	"go.uber.org/zap"
)

func NewZapLogger() (*zap.Logger, error) {
	return zap.NewDevelopment([]zap.Option{zap.AddCaller(), zap.AddCallerSkip(0)}...)
}

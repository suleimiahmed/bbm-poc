package config

import (
	"time"

	"github.com/spf13/viper"
)

type ConfigOverWrite map[string]any

type SyncBBM struct {
	// BBMMaxJobAttempts is the maximum amount of time to retry a a failed job.
	BBMMaxJobAttempts int
	// BBMMaxJobAttempts is the maximum amount of time to retry a a failed job.
	BBMMaxLockAcquistionAttempts int
	// BBMLockAcquistionInterval is the time to sleep between trying to acquire the lock.
	// This value should be significantly smaller than the interval it takes for the asychronous BBM process
	// To try to acquire the lock (i.e. AsyncBBM.JobInterval)
	BBMLockAcquistionInterval time.Duration
}

type AsyncBBM struct {
	// MaxJobAttempts is the number of times to retry a job before considering it failed
	MaxJobAttempts int
	// JobInterval is the duration between checking for a new BBM/ BBM job to be run
	JobInterval time.Duration
	// JobIntervalJitter is the random jitter applied to the JobInterval in seconds
	JobIntervalJitter int
}
type Config struct {
	DebugMode  bool
	DBName     string
	DBHost     string
	DBUser     string
	DBPassword string
	DBPort     string
	AsyncBBM   AsyncBBM
	SyncBBM    SyncBBM
}

func LoadConfig(overWrite ConfigOverWrite) (*Config, error) {
	config := viper.New()
	// set a default for environment variables
	config.SetDefault("DEBUG_MODE", true)
	config.SetDefault("DB_HOST", "localhost")
	config.SetDefault("DB_PORT", "5432")
	config.SetDefault("DB_NAME", "postgres")
	config.SetDefault("DB_USER", "postgres")
	config.SetDefault("DB_PASSWORD", "postgres")
	config.SetDefault("ASYNC_BBM_MAX_JOB_ATTEMPTS", 1)
	config.SetDefault("ASYNC_BBM_JOB_INTERVAL", 5*time.Second)
	config.SetDefault("ASYNC_BBM_JOB_INTERVAL_JITTER_SECONDS", 1)
	config.SetDefault("SYNC_BBM_MAX_JOB_ATTEMPTS", 5)
	config.SetDefault("SYNC_BBM_LOCK_ACQUISITION_ATTEMPTS", 100)
	config.SetDefault("SYNC_BBM_LOCK_ACQUISITION_INTERVAL", 10*time.Millisecond)

	// overwrites take precedence
	for key, val := range overWrite {
		config.Set(key, val)
	}

	// Load all envrionment variables
	config.AutomaticEnv()

	return &Config{
		DebugMode:  config.GetBool("DEBUG_MODE"),
		DBName:     config.GetString("DB_NAME"),
		DBHost:     config.GetString("DB_HOST"),
		DBPassword: config.GetString("DB_PASSWORD"),
		DBUser:     config.GetString("DB_USER"),
		DBPort:     config.GetString("DB_PORT"),
		AsyncBBM: AsyncBBM{
			config.GetInt("ASYNC_BBM_MAX_JOB_ATTEMPTS"),
			config.GetDuration("ASYNC_BBM_JOB_INTERVAL"),
			config.GetInt("ASYNC_BBM_JOB_INTERVAL_JITTER_SECONDS"),
		},
		SyncBBM: SyncBBM{
			config.GetInt("SYNC_BBM_MAX_JOB_ATTEMPTS"),
			config.GetInt("SYNC_BBM_LOCK_ACQUISITION_ATTEMPTS"),
			config.GetDuration("SYNC_BBM_LOCK_ACQUISITION_INTERVAL"),
		},
	}, nil
}
